# JavaScript: The Weird Part - Notes

## Vocabulary

### Syntax Parser

A program that reads your code and determines what it does an if its grammer is valid.

### Lexical Enviroment

Where something sits physically in the code you write.

### Execution Context

A wrapper to help manage the code that is running.

There are lts of lexical enviroments. Which one is currently running is managed via execution contexts. It can contain things beyond what you've written in your code.

### Name / Value Pair

A name which maps to a unique value. The name may be defined more than once, but only can have one value in any given context.

That value may be more name / value pairs.

### Object

A collection of name value pairs.

### Single Threaded

One command is being executed at a time.

### Synchronus

On at a time and in order.

### Invocation

Running/calling a function, using paranthesis.

### Variable Enviroment

Where the variables live and how they relate to each other in memory.

### Asynchronus

More than one at a time.

### Scope

Where a variable is availible in your code and if it's truly the same variable, or a new copy.

### Dynamic Typing

You dont tell the engine what type of data a variable holds, it figures it out while your code is running.

### Primitive Types

A type of data thaat repreisents a single value, that is, not an object.

* `undefined`
  * `undefined`represents a lack of existence. You shouln't set a variable to this.
* `null`
  * `null` represents a lack of existence. You can set a variable to this.
* `boolean`
  * A `boolean` represents either `true` or `false`.
* `number`
  * Floating point number (there's always some decimals). Unlike other programming languages, there's only one `number` type and it can make math weird.
* `string`
  * A sequence of characters, placed between single or double quotes.
* `symbol`
  * Used in ES6

### Operator

A special function that is syntactically (written) differently. Generally, operators take two parameters and return one result.

### Operator Precedence

Which operator function gets called first, when there's more than one on the same line of executable code. Functions are called in order of precedence. Higher wins.

<https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Operator_Precedence/>

### Operator Associativity

What order operator functions get called in: left-to-right, or right-to-left, when functions have the same precedence.

### Coercion

Converting a value from one type to another. This happens quite often in JavaScript because it's dynvamically typed.

### Namespace

A container for variables and functions. Typically to keep variables and functions with the same name seperate.

### First Class Functions

Evertyhing you can do with other types, you can do with functions. Assign them to variables, pass them around, create them on the fly.

### Function Expression

A unit of code that results in a value. It doesn't have to save a variable.

### Function Statement

Does work.

### By Value

Copy of value, at a different location in memory. Applies to all primitive types.

### By Reference

Points to same location in memory. All objects interact by refernce when setting them equal to each other or passing a function.

### Mutate

To change something. "Immutable means it can't be changed.

### Arguments

The parameters you pass to a function. JavaScript gives you a keyword of the same name which contains them all.

### Whitespace

Invisible characters that create literal space in your written code. Carriage returns, tabs, spaces etc.

### Callback Function

A function you give to another function, to be run when the other function is finished. So the function you invoke, calls back by calling the function you gave it when it finished.

### Function Currying

Creating a copy of a function but with some preset parameters. Very useful in mathamaterical situations.

### Inheritance

One object gets access to the properties and methods of another object.

### Reflection

An object can look at itself, listing and changing its properties and methods.

### Function Constructors

A normal function that is used to construct objects. The `this` variable points a new empty object, and that object is returned from the function automaiccally.

### Polyfill

Code that adds a feature which the engine _may_ lack.

### Syntactic Sugar

A different way to type something that doesn't change how it works under the hood.

## Method Chaining

Calling one method after another, and each method affects the paretnt object. So `obj.method1().method2()` where both methods end up with a `this` variable pointing at `obj`.

## The Execution Context: Creation and Hoisting

Exectuion context is created in two phases.

* The Creation Phase
  * Gloal Object
  * `this`
  * Outer Enviroment
  * Hoisting
  * Puts variables and functions in memory

* Code Execution
  * Runs code

## Function Invocation and The Exectuion Stack

Every execution stack has its own variable enviroment.





