console.log(this); // Window, the global object

function a() {
  console.log(this);
  this.newVariable = 'Hello'; // Window, the global object
}

var b = function() {
  console.log(this); // Window, the global object
};

a();

console.log(newVariable);

b();

var c = {
  name: 'The c object',
  log: function() {
    var self = this;

    this.name = 'Updated C object';
    console.log(this); // Points to the C object | 'Updated C object'

    var setName = function(newName) {
      this.name = newName; // Points to global object window
    };

    setName('Updated again!');
    console.log(this);
  },
};

c.log();

var c = {
  name: 'The c object',
  log: function() {
    var self = this; // Points to C | Use self everyhwere for sanity sake

    self.name = 'Updated C object';
    console.log(self);

    var setName = function(newName) {
      self.name = newName; // Points to C
    };

    setName('Updated again!');
    console.log(self);
  },
};

c.log();
