// eslint: airbnb
// Objective: Check if a letters from one string exist in another, in any order,
// irrespective of case.

function mutation(arr) {
  const test = arr[1].toLowerCase();
  const target = arr[0].toLowerCase();

  for (let i = 0; i < test.length; i++) {
    if (target.indexOf(test[i]) === -1) {
      return false;
    }
  }

  return true;
}

mutation(['hello', 'hey']);
