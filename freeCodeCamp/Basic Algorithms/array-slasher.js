// eslint: airbnb
// Objective: Remove n amount of elements from the head of an array, then return the remaining.

function slasher(arr, howMany) {
  return arr.slice(howMany);
}

slasher([1, 2, 3], 2);
