// Style: Airbnb

// Objective:
//  Return the provided string with the first letter of each word capitalized.
//  Make sure the rest of the word is in lower case.

function titleCase(str) {
  let x = str.toLowerCase();
  x = str.split(' ');

  for (let i = 0; i < x.length; i += 1) {
    x[i] = str[i].replace(x[i].charAt(0), x[i].charAt(0).toUpperCase());
  }

  x = x.join(' ');
  return x;
}

titleCase("I'm a little tea pot");
