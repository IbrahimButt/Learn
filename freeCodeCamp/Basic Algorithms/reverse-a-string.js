// Objective:
//      Reverse the string provided as an argument.

function reverseString(str) {
  let x = str.split('');
  x.reverse();
  x = x.join('');
  return str;
}

reverseString('hello');
