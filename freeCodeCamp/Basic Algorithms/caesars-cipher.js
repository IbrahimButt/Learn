// eslint: airbnb
// Objective: Implement Caesers cipher.

/* eslint-disable */

function rot13(str) { // LBH QVQ VG!
  var result = Array.prototype.map.call(str, function (char) {
    char = char.charCodeAt(0);
    if (char < 65 || char > 90) {
      return String.fromCharCode(char);
    } else if (char < 78) {
      return String.fromCharCode(char + 13);
    }
    return String.fromCharCode(char - 13);
  });

  return result.join('');
}

// Change the inputs below to test
rot13('SERR PBQR PNZC');
