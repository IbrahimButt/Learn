// eslint: airbnb
// Objective: Remove all falsy values from an array.

function bouncer(arr) {
  function truthyChecker(value) {
    if (value) {
      return value;
    }
    return false;
  }

  const filtered = arr.filter(truthyChecker);
  return filtered;
}

bouncer([7, 'ate', '', false, 9]);
