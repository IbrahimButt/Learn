function repeatStringNumTimes(str, num) {
  if (num > 0 ) {
    var originalStr = str;
    for (var i = 0; i < num - 1; i++) {
      str += originalStr;
    }
    return str;
  }
  return "";
}

repeatStringNumTimes("abc", 3);
