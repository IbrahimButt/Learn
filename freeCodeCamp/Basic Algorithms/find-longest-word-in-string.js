// Objective:
//      Return the length of the longest word in the provided sentence.

function findLongestWord(str) {
  const x = str.split(' ');
  let length = 0;
  for (let i = 0; i < x.length; i += 1) {
    if (x[i].length > length) {
      length = x[i].length;
    }
  }
  return length;
}

findLongestWord('The quick brown fox jumped over the lazy dog');
