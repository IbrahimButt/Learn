function fearNotLetter(str) {
  var startingCharCode = str.charCodeAt(0);
  for (var i = 0; i < str.length; i++)
    if (str.charCodeAt(i) !== startingCharCode + i)
      return String.fromCharCode(startingCharCode + i);
  return undefined;
}