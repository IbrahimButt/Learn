
function sumAll(arr) {
  let max = Math.max(...arr);
  const min = Math.min(...arr);
  let sum = 0;
  while (max >= min) {
    sum += max;
    max--;
  }
  return sum;
}

sumAll([1, 4]);
