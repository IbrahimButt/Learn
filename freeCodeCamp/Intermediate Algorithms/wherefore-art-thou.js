function whatIsInAName(collection, source) {

  var sourceKeys = Object.keys(source);

  // filter the collection
  return collection.filter(function (object) {
    for(var i = 0; i < sourceKeys.length; i++) {
      if(!object.hasOwnProperty(sourceKeys[i]) || object[sourceKeys[i]] !== source[sourceKeys[i]]) {
        return false;
      }
    }
    return true;
  });
}

whatIsInAName([{ first: "Romeo", last: "Montague" }, { first: "Mercutio", last: null }, { first: "Tybalt", last: "Capulet" }], { last: "Capulet" });