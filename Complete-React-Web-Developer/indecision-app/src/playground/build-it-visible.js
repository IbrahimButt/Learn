class VisibilityToggle extends React.Component {
  constructor(props) {
    super(props);
    this.handleToggle = this.handleToggle.bind(this);
    this.state = {
      visible: true
    }
  }

  handleToggle() {
    this.setState((prevState) => {
      return {
        visible: !prevState.visible
      }
    });
  }

  render() {
    return (
      <div>
        <h1>Toggle Visibility</h1>
        <button onClick={this.handleToggle}>{this.state.visible ? 'Hide Information' : 'Show Information'}</button>
        {this.state.visible && <p>The information.</p>}
      </div>
    )
  };
}

ReactDOM.render(<VisibilityToggle />, document.getElementById('app'));