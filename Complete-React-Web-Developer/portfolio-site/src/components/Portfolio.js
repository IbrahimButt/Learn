import React from 'react';
import {Link} from 'react-router-dom';

const Portfolio = () => (
  <div>
    <h2>Projects</h2>
    <Link to="/portfolio/1">Project One</Link>
    <Link to="/portfolio/2">Project two</Link>
    <Link to="/portfolio/3">Project Three</Link>
  </div>
);

export default Portfolio;