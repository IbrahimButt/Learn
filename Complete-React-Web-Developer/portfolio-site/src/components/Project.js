import React from 'react';

const Project = (props) => (
  <div>
    <h3>Thing I Did</h3>
    <p>Project id is {props.match.params.id}</p>
  </div>
);

export default Project;