// Object Destructuring

// const person = {
//   name: 'Ibrahim',
//   age: 24,
//   location: {
//     city: 'Glasgow',
//     temp: -2
//   }
// };

// // Set name to firstname, and then set default
// // Can also only use = to set default
// const { name: firstname = 'Anonymous', age } = person;
// console.log(`${firstname} is ${age}`);

// // Rename temp to temperature
// const { city, temp: temperature } = person.location;

// if (city && temperature) {
//   console.log(`It's ${temperature} celcius in ${city}`);
// }


// Array Destructuring

const address = ['1299 S Juniper Street', 'Philly', 'Penn', '19147'];
const [/* skip street */, city, state = 'NYC' , postCode] = address;
console.log(`You are in ${city}, ${state}.`);


const item = ['coffee (hot)', '$2.00', '$2.50', '$2.75'];
const [itemName, , mediumPrice] = item;
console.log(`A ${itemName} costs ${mediumPrice}.`);
