import moment from 'moment';
import filtersReducer from '../../reducers/filters';

test('Should setup default filter values.', () => {
  const state = filtersReducer(undefined, { type: '@@INIT' });
  expect(state).toEqual({
    text: '',
    sortBy: 'date',
    startDate: moment().startOf('month'),
    endDate: moment().endOf('month')
  });
});

test('Should set sortBy to amount.', () => {
  const state = filtersReducer(undefined, { type: 'SORT_BY_AMOUNT' });
  expect(state.sortBy).toBe('amount');
});

test('Should set sortBy to date.', () => {
  const currentState = {
    text: '',
    startDate: undefined,
    endDate: undefined,
    sortBy: 'amount'
  };
  const action = { type: 'SORT_BY_DATE' }
  const state = filtersReducer(currentState, action);
  expect(state.sortBy).toBe('date');
});

test('Should set text filter.', () => {
  const action = filtersReducer(undefined, {type: 'SET_TEXT_FILTER', text: 'hello'}); 
  expect(action.text).toBe('hello');
});

test('Should set text startDate filter.', () => {
  const action = filtersReducer(undefined, {type: 'SET_START_DATE', startDate: moment()}); 
  expect(action.startDate).toEqual(moment());
});

test('Should set text endDate filter.', () => {
  const action = filtersReducer(undefined, {type: 'SET_END_DATE', endDate: moment}); 
  expect(action.endDate).toEqual(moment);
});