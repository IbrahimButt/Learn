var currentInput = '';
var lastInput = '';
var completeInput = '';
var operator = '';

$('button').click(function () { // Get button pressed
  var digitButtonPressed = $(this).hasClass('digit');
  var operatorButtonPressed = $(this).hasClass('operator');
  var clearButtonPressed = $(this).attr('id') === 'clearButton';
  var ans = 0;

  if (clearButtonPressed) {
    currentInput = '';
    completeInput = '';
    lastInput = '';
    $('input[name=display]').val('');
  }
  
  if (digitButtonPressed) {


    if (completeInput.endsWith('='))
      completeInput = '';
    // Add digit pressed to currentInput and display
    currentInput += $(this).text();
    $('input[name=display]').val(eval(currentInput));

    // Add digit to complete input, which includes operators
    completeInput += $(this).text();

    // Save operator and currentInput, for when user presses = again after a previous result
    lastInput = operator + currentInput;
  }

  if (operatorButtonPressed && clearButtonPressed === false) {

    if (completeInput.endsWith('='))
      completeInput = completeInput.replace(/=/, '');

    if (/[\+\-\*\/]$/.test(completeInput)) {
      completeInput = completeInput.split('');
      completeInput.pop();
      completeInput.join('');
    }

    // Division operator needs stored as /, not as ÷
    if ($(this).attr('id') != 'equalsButton')
      $(this).attr('id') === 'divideButton' ? operator = '/' : operator = $(this).text();

    // If equal pressed, dont add it to completeInput, as .eval() won't work with '=' at the end
    // Just .eval() and display completeInput as is
    // If equal not prssed, add operator to completeInput
    // Therefore, the whole input is stored for being .eval()'d when needed
    if ($(this).attr('id') === 'equalsButton' && currentInput == false) {
      // Take the complete string, and onlyt add the previous operaotr and digits
      // Only applies when userpresses equal, without new input after a previous result
      if (/\d+[\+\-\*\/]+\d+/.test(completeInput)) // If.. what the shit ass
        completeInput += lastInput;

      $('input[name=display]').val(eval(completeInput));
    } else if ($(this).attr('id') === 'equalsButton') {
      $('input[name=display]').val(eval(completeInput));
      completeInput += $(this).text();
    } else if ($(this).attr('id') != 'equalsButton') {
      // Display a running total more than 1 operator pressed before equals
      $('input[name=display]').val(eval(completeInput));
      completeInput += operator;
    }
    // Reset currentInput as operator was pressed and new input expected.
    currentInput = '';
  }
});