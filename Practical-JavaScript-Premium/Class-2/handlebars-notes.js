// Create HTML code as a string
var htmlTemplate = '<div>{{ title }}</div>';
// Use Handlebars.js to turn it into actual HTML
var handlebarsTemplate = Handlebars.compile(htmlTemplate);

// Render template, with data passed in as an object through argument
handlebarsTemplate({ title: 'Hello' });

// ==========

// For Loop Syntax
var data = {todos: [{ title: 'First' }, { title: 'Second' }, { title: 'Third' }] };
var htmlTemplate = '<ul> {{ #todos }} <li>{{title}}</li> {{/todos}} </ul>';
var handlebarsTemplate = Handlebars.compile(htmlTemplate);
handlebarsTemplate(data);

// For Loop Syntax using this, as no object passed through, just an array that contains objects
// this refers to the array, so an object isnt needed to store the data in
var data = [{ title: 'First' }, { title: 'Second' }, { title: 'Third' }];
var htmlTemplate = '<ul> {{ #this }} <li>{{title}}</li> {{/this}} </ul>';
var handlebarsTemplate = Handlebars.compile(htmlTemplate);
handlebarsTemplate(data);

// ==========

// if statement syntax
var htmlTemplate = '<div>{{#if show}} TRUE {{/if}}</div>';
var handlebarsTemplate = Handlebars.compile(htmlTemplate);
handlebarsTemplate({show: true});