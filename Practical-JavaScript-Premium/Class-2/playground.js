todos = [{completed: true}, {completed: true}, {completed: false}];

function getActiveTodos() {
  return this.todos.filter(function (todo) {
    return !todo.completed;
  });
}

console.log(getActiveTodos());