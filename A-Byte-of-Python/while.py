number = 23
running = True

while running:
    guess = int(input('Enter an integer : '))
    # input here as if you were to ask before the while statement and the integer was below 23, the program will keep repeating, starting from below. The position the input is at right now, repeatedly asks for the input if its wrong. Basically, the loop is inside the while statment, so make sure to insert input in here.

    if guess == number:
        print('Congratulation, you guessed it.')
        # if correct guess, while loop stops
        running = False
    elif guess < number:
        print('No, it\'s a little higher than that.')
    else:
        print('No, it is a little lower than that.')
else:
    print('The while loop is over.')
# End of while statment blocks

print('Done')
