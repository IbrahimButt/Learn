while True:
    s = input('Enter something : ')
    if s == 'quit':
        break
    if len(s) < 3: # why not an elif statment? It's just an example, elif can be used, else cannot
        print('Too small')
        continue #in this example else: can be used
    print('Input is sufficient length')
