var size = 8;
var row = "";

for (var i = 0; i < size; i++) {
    var j = 0;
    while (j < size) {
        if ((i + j) % 2 == 1)
            row += "#";
        else
            row += " ";
        j++;
    }
    row += "\n";
}

console.log(row);