function getRange(start, end, step) {
  var range = [];
  if (step > 0) {
    for (var i = start; i <= end; i += step) {
      range.push(i);
    }
  } else {
    for (var i = start; i >= end; i += step) {
      range.push(i);
    }
  }
  return range;
}

function sumRange(range) {
  var sum = 0;
  for (var i = 0; i < range.length; i++) {
    sum += range[i];
  }
  return sum;
}

console.log(getRange(10, 1, -1));
