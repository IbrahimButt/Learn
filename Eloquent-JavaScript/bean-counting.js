function countBs(x) {
  return countChar(x, "B");
}

function countChar(x, y) {
  var count = 0;
  for (var i = 0; i < x.length; i++)
    if (x.charAt(i) === y)
      count++;
  return count;
}

console.log(countBs("BbBac"));
console.log(countChar("kakkerlak", "k"));