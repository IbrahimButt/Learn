function reverseArray(arr) {
  var newArray = [];
  arr.forEach(function(element) {
    return newArray.unshift(element);
  });
  return newArray;
}

console.log(reverseArray(["A", "B", "C"]));

function reverseArrayInPlace(arr) {
  var originalLength = arr.length;
  for (var i = 0; i < originalLength; i++) {
    arr.splice(arr.length - i, 0, arr[i]);
  }
  arr.splice(0, originalLength);
  return arr;
}

console.log(reverseArrayInPlace(["A", "B", "C"]));
