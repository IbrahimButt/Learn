function arrayToList(arr) {
  var list = {};
  arr.reverse().forEach(function(element) {
    list = { value: element, rest: list };
  });
  return list;
}

console.log(arrayToList([10, 20]));

function listToArray(list) {
  var arr = [];
  for (value in list) {
    if (list.hasOwnProperty(value)) {
      arr.push(list.value);
      list = list.rest;
    }
  }
  return arr;
}

console.log(listToArray(arrayToList([10, 20, 30])));

function prepend(value, list) {
  return {value: value, rest: list};
}

console.log(prepend(10, prepend(20)));

function nth(list, index) {
  var node = list;
  for (var i = 0; i <= index; i++) {
    if (i == index) {
      return node.value;
    }
    node = node.rest;
  }
}

console.log(nth(arrayToList([10, 20, 30]), 1));