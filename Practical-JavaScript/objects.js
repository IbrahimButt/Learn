// operatingSystem mac
// screenSize 15
// purchaseYear 2011


// make object
// property, value
var myComputer =
{
    operatingSystem: 'mac',
    screenSize: '15 inches',
    purchaseYear: 2011
};

// ----------

var ibrahim =
{
    name: 'Ibrahim',
    sayName: function()
    {
        console.log(this.name);
    }
}

ibrahim.sayName();