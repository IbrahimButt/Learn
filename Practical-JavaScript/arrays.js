// make todo list

var todos = ['item 1', 'item 2', 'item 3', 'item 4'];
console.log(todos)

//  add new todo

todos.push('item 5')
console.log(todos)

// edit exisitng todo

todos[0] = 'item 1 update'
console.log(todos)

// delete todo
// start from index 0 and delete 1 value
todos.splice(0, 1)
console.log(todos)

todos.splice(3, 1)
console.log(todos)