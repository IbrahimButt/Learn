// makeSandwichWith ___
//  get one slice of bread
//  add ___
//  put a slice of bread on top


// make function
// filling is a parameter
function makeSandwichWith(filling)
{
    // enter function steps
}

// call function example
// ham is an arguement
makeSandwichWith('ham');

// ----------

function sayHiTo(person)
{
    console.log('hi', person);
}

sayHiTo('Ibrahim'); // hi ibrahim

// ----------

// function to display todos
var todos = ['item 1', 'item 2', 'item 3'];

function displayTodos()
{
    console.log('My todos: ', todos);
}

displayTodos();

// function to add new todos

function addTodo(todo)
{
    todos.push(todo);
    displayTodos();
}

addTodo('some stuff');

// function to change todo

function changeTodo(position, newValue)
{
    todos[position] = newValue;
    displayTodos();
}

changeTodo(0, 'changed');
changeTodo(0, 'changed again')

// function to delete todo

function deleteTodo(position)
{
    todos.splice(position, 1);
    displayTodos();
}

deleteTodo(2);