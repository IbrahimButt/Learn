// mistakes deliberate

#include <stdlib.h>

void f(void)
{
    int *x = malloc(10 * sizeof(int));
    x[10] = 0;
    free(x);
}

int main(void)
{
    f();
    return 0;
}

// valgrind ./file
// valgrind --leak-check=full ./file 