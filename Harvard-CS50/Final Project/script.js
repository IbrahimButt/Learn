'use strict';

function calculate() {

  // store user input as variables
  const getGender = document.getElementById('genderSelect');
  let gender = getGender.options[getGender.selectedIndex].value;
  gender = parseInt(gender, 10);

  const age = document.getElementById('age').value;

  const height = document.getElementById('height').value;

  let weight = document.getElementById('weight').value;

  const getGoal = document.getElementById('goalSelect');
  let goal = getGoal.options[getGoal.selectedIndex].value;
  goal = parseInt(goal, 10);

  const getActivityLevel = document.getElementById('activityLevelSelect');
  let activityLevel = getActivityLevel.options[getActivityLevel.selectedIndex].value;
  activityLevel = parseInt(activityLevel, 10);

  // Calcualte REE
  let ree = 0;
  const reeStepOne = 10 * weight;
  const reeStepTwo = 6.25 * height;
  const reeStepThree = 5 * age;

  if (gender === 1) {
    ree = reeStepOne + reeStepTwo + reeStepThree + 5;
  } else if (gender === 2) {
    ree = reeStepOne + reeStepTwo + reeStepThree;
    ree -= 161;
  } else {
    console.log('Failed at REE calculation.');
    return false;
  }

  // Calculate TDEE
  let tdee = 0;
  if (activityLevel === 1) {
    tdee = ree * 1.2;
  } else if (activityLevel === 2) {
    tdee = ree * 1.375;
  } else if (activityLevel === 3) {
    tdee = ree * 1.55;
  } else if (activityLevel === 4) {
    tdee = ree * 1.725;
  } else {
    console.log('Failed at TDEE calcualtion.');
    return false;
  }

  // Calculate TDEE for goal
  if (goal === 1) {
    tdee -= (tdee * 0.20);
  } else if (goal === 2) {
    tdee += (tdee * 0.20);
  } else if (goal === 3) {
    return true; // Maintainace goal requires base TDEE
  } else {
    console.log('Failed at TDEE goal calcualtion.');
    return false;
  }

  // Calculate Macros
  // Convert KG to lbs
  weight *= 2.20462;
  const protien = weight * 0.825;

  const fat = (tdee * 0.25) / 9;

  // var carbs = tdee - (protien * 4) + (fat * 9);
  // carbs = carbs / 4;

  let carbs = (protien * 4) + (fat * 9);
  carbs = tdee - carbs;
  carbs /= 4;

  console.log('Finished successfully.');

  document.getElementById('result').innerHTML = `<p>You should consume ${Math.round(tdee)} calories, consisting of ${Math.round(protien)}g of protien, ${Math.round(carbs)}g of carbohydrates and ${Math.round(fat)}g of fat.</p>`;
}
